import { Application, Sprite, SCALE_MODES, Text, TextStyle } from './pixi.js';
import HorizontalSlider from './src/slider/HorizontalSlider.js';
import PercentagesRule from './src/slider/calculation-rules/PercentagesRule.js';
import RangeRule from './src/slider/calculation-rules/RangeRule.js';
import VerbalRule from './src/slider/calculation-rules/VerbalRule.js';


const app = new Application();
document.body.appendChild(app.view);

app.loader
	.add('slider_button', './assets/slider_button.png')
	.add('slider_foreground', './assets/slider_foreground.png')
	.add('slider_background', './assets/slider_background.png')
    .load(run);

function run()
{
	const style = {fill: ['#ffffff']}
	const rangeText = new Text('-1', new TextStyle(style));
	rangeText.anchor.set(.5);
	rangeText.x = 370;
	rangeText.y = 340;
	app.stage.addChild(rangeText);

	const rangeText1 = new Text('-1', new TextStyle(style));
	rangeText1.x = 70;
	rangeText1.y = 284;
	app.stage.addChild(rangeText1);

	const percentagesText = new Text('-1', new TextStyle(style));
	percentagesText.x = 644;
	percentagesText.y = 284;
	app.stage.addChild(percentagesText);

	const verbalText = new Text('-1', new TextStyle(style));
	verbalText.anchor.set(.5);
	verbalText.x = 370;
	verbalText.y = 260;
	app.stage.addChild(verbalText);

	const slider = new HorizontalSlider(
		new Sprite(app.loader.resources.slider_button.texture), 
		new Sprite(app.loader.resources.slider_foreground.texture),
		new Sprite(app.loader.resources.slider_background.texture),
		400,
		20
	);

	slider.x = 180;
	slider.y = 300;
	app.stage.addChild(slider);

	slider
		.addCalculationFlow(new PercentagesRule(400), value => percentagesText.text = value + '%')
		.addCalculationFlow(new RangeRule(400, -1, 1), value => rangeText.text = value.toFixed(2))
		.addCalculationFlow(new RangeRule(400, 2020, 4717), value => rangeText1.text = Math.round(value))
		.addCalculationFlow(new VerbalRule(400), value => verbalText.text = value);

}
