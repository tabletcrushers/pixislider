import { Container, Graphics, Rectangle } from '../../pixi.js';

export default class HorizontalSlider extends Container {
	constructor(knob, foreground, background, lenght, bacgroundWidth, orientation) {
		super();
		
		this._knob = knob;
		this._foreground = foreground;
		this._background = background;
		
		this._bacgroundWidth = bacgroundWidth;
		this._lenght = lenght;
		this._maxGap = 40;

		this._ruleCallbackPair = [];

		this._init();
	}

	addCalculationFlow(rule, callback) {
		this._ruleCallbackPair.push({rule, callback});
		callback(rule.calculate(this._knob.x));

		return this;
	}

	subscribe() {
		this._knob
			.on('pointerdown', this._onDragStarted, this)
			.on('pointerup', this._onDragFinished, this)
			.on('pointerupoutside', this._onDragFinished, this)
			.on('pointermove', this._onDragged, this);

		this._background.on('pointerup', this._onForegroundPressed, this);
	}

	unsubscribe() {
		this._knob
			.off('pointerdown', this._onDragStarted)
			.off('pointerup', this._onDragFinished)
			.off('pointerupoutside', this._onDragFinished)
			.off('pointermove', this._onDragged);

		this._background.off('pointerup', this._onForegroundPressed);
	}

	_init() {
		this._knob.anchor.set(.5);
		this._knob.interactive = true;
		this._knob.buttonMode = true;

		this._foreground.anchor.y = .5;

		this._background.anchor.y = .5;
		this._background.hitArea = new Rectangle(0, -this._bacgroundWidth / 2, this._lenght, this._bacgroundWidth)
		this._background.interactive = true;
		this._background.buttonMode = true;

		this._foreground.mask = this._drawArea();
		this._foreground.mask.x = -this._lenght;
		this._foreground.mask.y = -this._bacgroundWidth / 2;

		this.addChild(this._background);
		this.addChild(this._foreground);
		this.addChild(this._foreground._mask);
		this.addChild(this._knob);

		this.subscribe();
	}

	_onForegroundPressed(event) {
		this._pointerData = event.data;
		this._processDragging(this._pointerData.getLocalPosition(this));
	}

	_onDragStarted(event) {
		this._pointerData = event.data;
		this._knob.scale.set(1.2);
		this._isDragging = true;
	}

	_onDragFinished() {
		this._isDragging = false;
		this._knob.scale.set(1);
	}

	_onDragged() {
		if (this._isDragging) {
			this._processDragging(this._pointerData.getLocalPosition(this));
		}
	}

	_processDragging(newPosition) {
		if (newPosition.x < 0) {
			newPosition.x  = 0;
		}
		else if (newPosition.x > this._lenght) {
			newPosition.x = this._lenght;
		}
		
		this._knob.x = newPosition.x;
		this._foreground._mask.x = newPosition.x - this._lenght;

		this._callRules(newPosition.x);
		
		if (this._gapLimitExceeded(this._pointerData.getLocalPosition(this))) {
			this._onDragFinished();
		}
	}

	_callRules(value) {
		for (const pair of this._ruleCallbackPair) {
			pair.callback(pair.rule.calculate(value));
		}
	}

	_gapLimitExceeded(value) {
		const p1 = value;
		const p2 = { x: this._knob.x, y: this._knob.y };

		return Math.sqrt(Math.pow((p1.x - p2.x), 2) + Math.pow((p1.y - p2.y), 2)) > this._maxGap;
	}

	_drawArea() {
		const mask = new Graphics();
		mask.beginFill(0xffffff, 1);
		mask.drawRect(0, 0, this._lenght, this._bacgroundWidth);
		mask.endFill();

		return mask;
	}
}