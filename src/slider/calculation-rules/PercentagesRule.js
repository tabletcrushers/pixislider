export default class PercentagesRule {
	constructor(pathLenght) {
		this._onePercent = pathLenght / 100;
	}

	calculate(value) {

		return Math.round(value / this._onePercent);
	}
}