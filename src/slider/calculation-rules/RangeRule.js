export default class RangeRule {
	constructor(pathLenght, min, max) {
		this._min = min;
		this._oneUnit = pathLenght / (max - min);
	}

	calculate(value) {

		return this._min + value / this._oneUnit;
	}
}