export default class VerbalRule {
	constructor(pathLenght) {
		this._pathLenght = pathLenght;
	}

	calculate(value) {
		
		if (value === 0) return 'nothing';
		else if (value === this._pathLenght) return 'whole';
		else if (value > this._pathLenght / 2) return 'more then half';
		else if (value < this._pathLenght / 2) return 'less then half';
	}
}